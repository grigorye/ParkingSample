//
//  VehicleDetailViewController.swift
//  ParkingSample
//
//  Created by Grigory Entin on 15.03.2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import UIKit

class VehicleDetailViewController : UITableViewController {
	
	@IBOutlet weak var vrnLabel: UILabel!
	@IBOutlet weak var titleLabel: UILabel!

	func configureView() {
		// Update the user interface for the detail item.
		if let detail = detailItem {
			if let label = vrnLabel {
				label.text = detail.vrn
			}
			if let label = titleLabel {
				label.text = detail.title
			}
		}
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		configureView()
	}
	
	var detailItem: Vehicle? {
		didSet {
			// Update the view.
			configureView()
		}
	}
}

