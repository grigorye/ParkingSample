//
//  NewUserViewController.swift
//  ParkingSample
//
//  Created by Grigory Entin on 15.03.2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import UIKit
import CoreData

class NewUserViewController : UserDetailBaseViewController {

	var temporaryCachedREST: CachedREST!
	var parentCachedREST: CachedREST!

	override var cachedREST: CachedREST! {
		set {
			parentCachedREST = newValue
		}
		get {
			return temporaryCachedREST
		}
	}
	
	func saveNewUser() {
		do {
			try cachedREST.context.save()
			try parentCachedREST.context.save()
		} catch {
			app.reportError(error)
		}
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		let temporaryContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
		temporaryContext.parent = parentCachedREST.context
		temporaryCachedREST = CachedREST(cache: FlexibleCache(context: temporaryContext), baseURL: parentCachedREST.baseURL)
		
		user = User(context: temporaryContext) … {
			$0.hasPrimitiveData = true
		}
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		super.prepare(for: segue, sender: sender)
		
		switch x$(segue.identifier) {
		case "unwindForNewUser"?:
			saveNewUser()
		default:()
		}
	}
}
