//
//  StopParkingViewController.swift
//  ParkingSample
//
//  Created by Grigory Entin on 22/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import UIKit

class StopParkingViewController : UIViewController, CachedRESTConsumer, UserConsumer, ParkingZoneConsumer, VehicleConsumer, ParkingActionConsumer {
	
	var cachedREST: CachedREST!
	var user: User!
	
	var parkingZone: ParkingZone!
	var vehicle: Vehicle!

	// Should match parkingZone and vehicle
	var parkingAction: ParkingAction!

	var stopParkingHandler: (() -> Void)!
	
	@IBOutlet var popupContainerView: UIView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
	}
	
	@IBAction func stopParking() {
		let stoppedPrimitiveParkingAction = parkingAction.cachedPrimitiveObject()! … {
			$0.stopDate = Date()
		}
		cache.update(ParkingAction.self, of: parkingAction!, from: stoppedPrimitiveParkingAction) {
			dispatch($0, catch: { error in
				app.reportError(x$(error))
			}, or: {
				self.stopParkingHandler()
			})
		}
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		pushModel(to: segue)
		switch segue.identifier {
		case "embedPopup"?:
			let parkingActionPopupViewController = segue.destination as! ParkingActionPopupViewController
			parkingActionPopupViewController … {
				$0.actionTitle = NSLocalizedString("Stop Parking", comment: "")
				$0.parkingActionHandler = { [weak self] in
					self?.stopParking()
				}
			}
		default:
			()
		}
	}
}
