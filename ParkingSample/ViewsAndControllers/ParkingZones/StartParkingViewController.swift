//
//  StartParkingViewController.swift
//  ParkingSample
//
//  Created by Grigory Entin on 22/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import UIKit

class StartParkingViewController : ViewModelControllerBase, ModelController, CachedRESTConsumer, UserConsumer, ParkingZoneConsumer, VehicleConsumer, ParkingActionConsumer {
	
	var cachedREST: CachedREST!
	@objc dynamic var user: User!
	@objc dynamic var parkingZone: ParkingZone!
	@objc dynamic var parkingAction: ParkingAction!

	@objc static var keyPathsForValuesAffectingVehicle: Set<String> {
		return [
			#keyPath(user.defaultVehicle),
			#keyPath(selectedVehicle)
		]
	}
	@objc dynamic var vehicle: Vehicle! {
		set {
			assert(false)
		}
		get {
			return selectedVehicle ?? user.defaultVehicle
		}
	}
	
	@objc dynamic var selectedVehicle: Vehicle!
	@objc dynamic var shouldHidePopup = true

	// MARK: -
	
	func modelDidChange() {
		_ = x$(self)
		
		do {
			let shouldHidePopup = self.shouldHidePopup
			if popupContainerView.isHidden != shouldHidePopup {
				UIView.animate(withDuration: 0.3) {
					self.popupContainerView.isHidden = shouldHidePopup
				}
			}
		}
	}
	
	@objc static var keyPathsForValuesAffectingModel: Set<String> {
		return [
			#keyPath(vehicle),
			#keyPath(parkingZone),
			#keyPath(vehicle),
			#keyPath(parkingAction),
			#keyPath(shouldHidePopup),
		]
	}
	
	// MARK: -

	@IBOutlet var mapContainerView: UIView!
	@IBOutlet var popupContainerView: UIView!
	
	// MARK: -
	
	func mapViewDidSelect(_ parkingZone: ParkingZone?) {
		shouldHidePopup = parkingZone == nil
		guard let parkingZone = parkingZone else {
			return
		}
		self.parkingZone = parkingZone
	}
	
	@IBAction func startParking() {
		let primitiveParkingAction = PrimitiveParkingAction(
			userIdOrUser: .cachedObject(user),
			vehicleId: vehicle!.remoteObjectId!,
			zoneId: parkingZone!.remoteObjectId!,
			startDate: Date(),
			stopDate: nil
		)
		cache.create(ParkingAction.self, from: primitiveParkingAction) {
			dispatch($0, catch: { error in
				app.reportError(x$(error))
			}, or: { (parkingAction) in
				self.parkingAction = x$(parkingAction)
				self.performSegue(withIdentifier: "showStopParking", sender: nil)
			})
		}
	}
	
	// MARK: -
	
	@IBAction func unwindForVehicleSelection(_ segue: UIStoryboardSegue) {
	}
	
	@IBAction func unwindForStopParking(_ segue: UIStoryboardSegue) {
	}

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		pushModel(to: segue)
		switch segue.identifier {
		case "embedMap":
			(segue.destination as! ParkingZoneMapViewController) … {
				$0.selectionHandler = { [weak self] parkingZone in
					self!.mapViewDidSelect(parkingZone)
				}
			}
		case "embedPopup":
			(segue.destination as! ParkingActionPopupViewController) … {
				$0.actionTitle = NSLocalizedString("Start Parking", comment: "")
				$0.parkingActionHandler = { [weak self] in
					self!.startParking()
				}
				$0.vehicleSelectionHandler = { [weak self] in
					self!.performSegue(withIdentifier: "changeVehicle", sender: nil)
				}
			}
		case "showStopParking":
			(segue.destination.descendant() as StopParkingViewController) … {
				let x = $0
				$0.stopParkingHandler = { [unowned x, weak self] in
					x.performSegue(withIdentifier: "unwindForStopParking", sender: nil)
					let tabBarController = self!.tabBarController!
					tabBarController.selectedViewController = (tabBarController.descendant() as HistoryViewController).navigationController!
				}
			}
		case "changeVehicle":
			(segue.destination.descendant() as VehicleListViewController) … {
				$0.selectedVehicle = vehicle
				let x = $0
				$0.selectionHandler = { [weak x, weak self] (vehicle) in
					self!.selectedVehicle = vehicle
					x!.performSegue(withIdentifier: "unwindForVehicleSelection", sender: nil)
				}
			}
		default:
			()
		}
	}
	
	deinit {
		_ = x$(self)
	}
}
