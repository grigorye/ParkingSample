//
//  ParkingZone+MKAnnotation.swift
//  ParkingSample
//
//  Created by Grigory Entin on 22/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import MapKit
import Foundation

extension ParkingZone : MKAnnotation {
	
	public var coordinate: CLLocationCoordinate2D {
		return CLLocationCoordinate2D(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(lon))
	}
}
