//
//  ParkingActionPopupViewController.swift
//  ParkingSample
//
//  Created by Grigory Entin on 23/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import UIKit

class ParkingActionPopupViewController : TableViewModelControllerBase, ModelController, ParkingZoneConsumer, VehicleConsumer {
	
	var parkingActionHandler: (() -> Void)!
	@objc dynamic var vehicleSelectionHandler: (() -> Void)?
	var actionTitle: String!
	
	@objc dynamic var parkingZone: ParkingZone!
	@objc dynamic var vehicle: Vehicle!
	
	@IBOutlet var parkingZoneLabel: UILabel!
	@IBOutlet var vehicleLabel: UILabel!
	@IBOutlet var vehicleCell: UITableViewCell!
	@IBOutlet var parkingActionCell: UITableViewCell!
	
	// MARK: -

	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		switch tableView.cellForRow(at: indexPath) {
		case vehicleCell:
			vehicleSelectionHandler?()
		case parkingActionCell:
			parkingActionHandler?()
		default:
			()
		}
	}
	
	// MARK: -
	
	func modelDidChange() {
		vehicleLabel.text = localizedDisplayName(for: vehicle)
		parkingZoneLabel.text = parkingZone?.address ?? NSLocalizedString("None", comment: "")
		vehicleCell.selectionStyle = (vehicleSelectionHandler == nil) ? .none : .default
		parkingActionCell.selectionStyle = (vehicle != nil) ? .default : .none
		parkingActionCell.textLabel!.textColor = (vehicle != nil) ? nil : .lightGray
		parkingActionCell.isUserInteractionEnabled = (vehicle != nil)
	}
	
	@objc static var keyPathsForValuesAffectingModel: Set<String> {
		return [
			#keyPath(parkingZone.address),
			#keyPath(vehicle.title),
			#keyPath(vehicleSelectionHandler)
		]
	}

	// MARK: -
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		parkingActionCell.textLabel!.text = actionTitle
	}
	
	// MARK: -
	
	deinit {
		_ = x$(self)
	}
}
