//
//  ParkingZoneMapViewController.swift
//  ParkingSample
//
//  Created by Grigory Entin on 21/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import UIKit
import MapKit
import CoreData

class ParkingZoneMapViewController : UIViewController, CachedRESTConsumer {
	
	var cachedREST: CachedREST!
	
	@IBOutlet var mapView: MKMapView!
	@IBOutlet var userTrackingButtonContainerView: UIView!
	
	var selectionHandler: ((ParkingZone?) -> Void)?
	
	func trackRefreshFailed(_ error: Error) {
		_ = x$(error)
	}
	
	private lazy var mapViewFetchedResultsControllerDelegate: MapViewFetchedResultsControllerDelegate = {
		return MapViewFetchedResultsControllerDelegate(for: mapView!)
	}()
	
	private lazy var fetchedResultsController: NSFetchedResultsController<ParkingZone> = {
		let fetchRequest: NSFetchRequest<ParkingZone> = ParkingZone.fetchRequest() … {
			$0.sortDescriptors = [NSSortDescriptor(key: #keyPath(ParkingZone.remoteObjectId), ascending: true)]
		}
		let context = cache.context
		return NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil) … {
			$0.delegate = mapViewFetchedResultsControllerDelegate
		}
	}()
	
	let locationManager = CLLocationManager()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		let userTrackingButton = MKUserTrackingButton(mapView: mapView)
		userTrackingButtonContainerView.addSubview(userTrackingButton)
		userTrackingButton … {
			$0.frame = $0.superview!.bounds
		}
		mapView.delegate = self
		
		scheduledForViewWillAppear.append { [weak self] in
			self!.onceViewWillAppear()
		}
	}
	
	private func prepareFetchedResults() {
		try! fetchedResultsController.performFetch()
		mapViewFetchedResultsControllerDelegate.controllerDidFetchObjects(fetchedResultsController)
	}
	
	func onceViewWillAppear() {
		prepareFetchedResults()
	}
	
	private var scheduledForViewWillAppear = [() -> Void]()
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		scheduledForViewWillAppear.forEach { $0() }
		scheduledForViewWillAppear = []
		
		cachedREST.refreshAll(ParkingZoneCachedRESTEntity.self) {
			dispatch($0, catch: { [weak self] error in
				self?.trackRefreshFailed(error)
			}, or: { [weak self] in
				_ = x$(self)
			})
		}
	}
	
	deinit {
		_ = x$(self)
	}
}

extension ParkingZoneMapViewController : MKMapViewDelegate {
	
	func mapView(_ mapView: MKMapView, didFailToLocateUserWithError error: Error) {
		_ = x$(error)
	}
	
	func mapViewWillStartLocatingUser(_ mapView: MKMapView) {
		let locationServicesAuthorizationStatus = CLLocationManager.authorizationStatus()
		switch locationServicesAuthorizationStatus {
		case .notDetermined:
			locationManager.requestWhenInUseAuthorization()
		case .denied:
			_ = x$(string(from: locationServicesAuthorizationStatus))
		default:
			_ = x$(string(from: locationServicesAuthorizationStatus))
		}
	}
	
	func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
		selectionHandler?(view.annotation as! ParkingZone?)
	}
	
	func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
		selectionHandler?(nil)
	}
}
