//
//  UserDisplayStrings.swift
//  ParkingSample
//
//  Created by Grigory Entin on 25/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import Foundation

func localizedDisplayName(for vehicle: Vehicle?) -> String {
	guard let vehicle = vehicle else {
		return NSLocalizedString("Unset", comment: "")
	}
	guard let vehicleTitle = vehicle.title else {
		assert(!vehicle.hasPrimitiveData)
		return NSLocalizedString("Not Loaded", comment: "")
	}
	return vehicleTitle
}
