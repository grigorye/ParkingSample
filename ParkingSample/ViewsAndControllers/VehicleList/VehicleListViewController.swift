//
//  VehicleListViewController.swift
//  ParkingSample
//
//  Created by Grigory Entin on 15.03.2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import CoreData
import UIKit

class VehicleListViewController : UITableViewController, CachedRESTConsumer, UserConsumer {
	
	var cachedREST: CachedREST!
	var user: User!
	
	var selectedVehicle: Vehicle? {
		didSet {
			guard nil != viewIfLoaded else {
				return
			}
			if let vehicle = selectedVehicle {
				if let cell = tableView.cellForRow(at: fetchedResultsController.indexPath(forObject: vehicle)!) {
					configureCell(cell, withVehicle: vehicle)
				}
			}
			if let vehicle = oldValue {
				if let cell = tableView.cellForRow(at: fetchedResultsController.indexPath(forObject: vehicle)!) {
					configureCell(cell, withVehicle: vehicle)
				}
			}
		}
	}
	var selectionHandler: ((Vehicle) -> Void)!
	
	private var detailViewController: VehicleDetailViewController? = nil

	// MARK: -
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		let selectedVehicle = fetchedResultsController.object(at: indexPath)
		if x$(tableView.isEditing) {
			performSegue(withIdentifier: "showDetail", sender: nil)
		} else {
			self.selectedVehicle = selectedVehicle
			selectionHandler(selectedVehicle)
		}
	}
	
	// MARK: -
	
	func onceViewWillAppear() {
		try! fetchedResultsController.performFetch()
		cachedREST.refreshAll(VehicleCachedRESTEntity.self) {
			dispatch($0, catch: { error in
				_ = x$(error)
			}, or: {
				_ = x$(true)
			})
		}
	}
	
	// MARK: -

	override func viewDidLoad() {
		super.viewDidLoad()
		
		assert(tableView.delegate === self)
		tableView.dataSource = tableViewDataSource
		tableView.register(UINib(nibName: "VehicleCell", bundle: nil), forCellReuseIdentifier: "Cell")

		navigationItem.leftItemsSupplementBackButton = true
		navigationItem.leftBarButtonItem = editButtonItem
		
		let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(newVehicle(_:)))
		navigationItem.rightBarButtonItem = addButton
		if let split = splitViewController {
			let controllers = split.viewControllers
			detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? VehicleDetailViewController
		}
		
		scheduledForViewWillAppear.append { [weak self] in
			self?.onceViewWillAppear()
		}
	}
	
	var scheduledForViewWillAppear = [() -> Void]()
	
	override func viewWillAppear(_ animated: Bool) {
		//clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
		super.viewWillAppear(animated)
		
		scheduledForViewWillAppear.forEach { $0() }
		scheduledForViewWillAppear = []
	}

	// MARK: -

	@IBAction func newVehicle(_ sender: Any) {
		performSegue(withIdentifier: "newVehicle", sender: self)
	}
	
	// MARK: -
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		pushModel(to: segue)
		switch segue.identifier {
		case "showDetail"?:
			if let indexPath = tableView.indexPathForSelectedRow {
				let object = fetchedResultsController.object(at: indexPath)
				let vehicleDetailViewController = (segue.destination as! UINavigationController).topViewController as! VehicleDetailViewController
				vehicleDetailViewController … {
					$0.detailItem = object
					$0.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
					$0.navigationItem.leftItemsSupplementBackButton = true
				}
			}
		case "newVehicle"?:
			let navigationController = segue.destination as! UINavigationController
			let newVehicleViewController = navigationController.viewControllers.first as! NewVehicleViewController
			newVehicleViewController … {
				// Unwind segue?
				$0.doneHandler = { [unowned self] in
					self.dismiss(animated: true)
				}
				$0.cancelHandler = {
					self.dismiss(animated: true)
				}
			}
		default:
			()
		}
	}
	
	// MARK: -

	func deleteVehicle(_ vehicle: Vehicle) {
		cachedREST.delete(VehicleCachedRESTEntity.self, for: vehicle) {
			dispatch($0, catch: {
				app.reportError($0)
			}, or: {
				_ = x$(vehicle)
			})
		}
	}
	
	func configureCell(_ cell: UITableViewCell, withVehicle vehicle: Vehicle) {
		cell.textLabel!.text = vehicle.vrn
		cell.detailTextLabel!.text = vehicle.title
		cell.accessoryType = (selectedVehicle == vehicle) ? .checkmark : .none
	}
	
	// MARK: -
	
	lazy var tableViewDataSource = FetchedResultsControllerTableViewDataSource(fetchedResultsController: fetchedResultsController, configureCell: configureCell, deleteObject: deleteVehicle)
	lazy var fetchedResultsControllerDelegate = TableViewFetchedResultsControllerDelegate(tableView: tableView, configureCell: self.configureCell)
	
	lazy var fetchedResultsController: NSFetchedResultsController<Vehicle> = {
		let fetchRequest: NSFetchRequest<Vehicle> = Vehicle.fetchRequest() … {
			$0.fetchBatchSize = 20
			$0.sortDescriptors = [NSSortDescriptor(key: #keyPath(Vehicle.createdAt), ascending: true)]
			$0.predicate = NSPredicate(format: "%K = %@", #keyPath(Vehicle.user), user!)
		}
		
		return NSFetchedResultsController(fetchRequest: x$(fetchRequest), managedObjectContext: cache.context, sectionNameKeyPath: nil, cacheName: nil) … {
			$0.delegate = fetchedResultsControllerDelegate
		}
	}()
}
