//
//  NewVehicleViewController.swift
//  ParkingSample
//
//  Created by Grigory Entin on 15.03.2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import UIKit

class NewVehicleViewController : UITableViewController, CachedRESTConsumer, UserConsumer {
	
	var cachedREST: CachedREST!
	var user: User!
	
	var doneHandler: (() -> Void)!
	var cancelHandler: (() -> Void)!

	@IBOutlet var vrnField: UITextField!
	@IBOutlet var titleField: UITextField!

	@IBAction func done(_: Any) {
		let primitiveVehicle = PrimitiveVehicle(
			userIdOrUser: .cachedObject(user),
			vrn: vrnField.text,
			title: titleField.text,
			isDefault: nil
		)
		cache.create(Vehicle.self, from: primitiveVehicle) {
			dispatch($0, catch: { error in
				app.reportError(x$(error))
			}, or: { vehicle in
				self.doneHandler?()
				_ = x$(vehicle)
			})
		}
	}
	
	@IBAction func cancel(_: Any) {
		cancelHandler?()
	}
}
