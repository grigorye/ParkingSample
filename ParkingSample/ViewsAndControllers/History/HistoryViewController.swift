//
//  VehicleListViewController.swift
//  ParkingSample
//
//  Created by Grigory Entin on 15.03.2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import CoreData
import UIKit

let dateFormatter = DateFormatter()

class HistoryViewController : UITableViewController, CachedRESTConsumer, UserConsumer {
	
	var cachedREST: CachedREST!
	var user: User!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		tableView.dataSource = tableViewDataSource
		
		tableView.register(UINib(nibName: "HistoryCell", bundle: nil), forCellReuseIdentifier: "Cell")

		navigationItem.rightBarButtonItem = editButtonItem
		
		scheduledForViewWillAppear.append { [weak self] in
			self?.onceViewWillAppear()
		}
	}
	
	// MARK: -
	
	@IBAction func refresh() {
		let refreshControl = self.refreshControl!
		assert(refreshControl.isRefreshing)
		cachedREST.refreshAll(ParkingActionCachedRESTEntity.self) {
			DispatchQueue.main.async {
				refreshControl.endRefreshing()
			}
			dispatch($0, catch: { error in
				_ = app.reportError(error)
			}, or: {
				_ = x$($0)
			})
		}
	}
	
	func triggerRefresh() {
		refreshControl!.beginRefreshing()
		refresh()
	}
	
	// MARK: -
	
	func onceViewWillAppear() {
		try! fetchedResultsController.performFetch()
		triggerRefresh()
	}

	var scheduledForViewWillAppear = [() -> Void]()
	
	override func viewWillAppear(_ animated: Bool) {
		//clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
		super.viewWillAppear(animated)
		
		scheduledForViewWillAppear.forEach { $0() }
		scheduledForViewWillAppear = []
	}
	
	// MARK: -
	
	func deleteParkingAction(_ parkingAction: ParkingAction) {
		cachedREST.delete(ParkingActionCachedRESTEntity.self, for: parkingAction) {
			dispatch($0, catch: {
				app.reportError($0)
			}, or: {
				_ = x$(parkingAction)
			})
		}
	}
	
	func configureCell(_ cell: UITableViewCell, with parkingAction: ParkingAction) {
		let vehicle = user.vehicleWithId(parkingAction.vehicleId!)
		cell.textLabel!.text = String(format: NSLocalizedString("%@ - %@", comment: ""), parkingAction.zoneId!, localizedDisplayName(for: vehicle))
		let dateText = dateFormatter.string(from: parkingAction.startDate!)
		cell.detailTextLabel!.text = dateText
	}
	
	lazy var tableViewDataSource = FetchedResultsControllerTableViewDataSource(fetchedResultsController: fetchedResultsController, configureCell: configureCell, deleteObject: deleteParkingAction)
	lazy var fetchedResultsControllerDelegate = TableViewFetchedResultsControllerDelegate(tableView: tableView, configureCell: self.configureCell)
	
	lazy var fetchedResultsController: NSFetchedResultsController<ParkingAction> = {
		let fetchRequest: NSFetchRequest<ParkingAction> = ParkingAction.fetchRequest() … {
			$0.fetchBatchSize = 20
			$0.sortDescriptors = [NSSortDescriptor(key: #keyPath(ParkingAction.startDate), ascending: false)]
			$0.predicate = NSPredicate(format: "%K = %@", #keyPath(ParkingAction.user), user)
		}
		
		return NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil) … {
			$0.delegate = fetchedResultsControllerDelegate
		}
	}()

	deinit {
		_ = x$(self)
	}
}
