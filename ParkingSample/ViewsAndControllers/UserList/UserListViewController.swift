//
//  MasterViewController.swift
//  ParkingSample
//
//  Created by Grigory Entin on 15.03.2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import UIKit
import CoreData

extension User {
	fileprivate var displayName: String {
		switch (firstName, lastName) {
		case (let firstName?, let lastName?):
			return String(format: NSLocalizedString("%@ %@", comment: "User display name format"), firstName, lastName)
		case (let firstName?, nil):
			return firstName
		case (nil, let lastName?):
			return lastName
		case (nil, nil):
			return NSLocalizedString("Unknown", comment: "User display name for no last and first name")
		}
	}
}

class UserListViewController: UITableViewController, CachedRESTConsumer {

	var cachedREST: CachedREST!
	
	var detailViewController: UserDetailViewController? = nil

	override func viewDidLoad() {
		super.viewDidLoad()
		
		tableView.dataSource = tableViewDataSource
		
		navigationItem.leftBarButtonItem = editButtonItem

		let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(newUser(_:)))
		navigationItem.rightBarButtonItem = addButton
		if let split = splitViewController {
		    let controllers = split.viewControllers
		    detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? UserDetailViewController
		}
		
		scheduledForViewWillAppear.append { [weak self] in
			self?.onceViewWillAppear()
		}
	}
	
	@IBAction func refresh() {
		let refreshControl = self.refreshControl!
		assert(refreshControl.isRefreshing)
		cachedREST.refreshAll(UserCachedRESTEntity.self) {
			DispatchQueue.main.async {
				refreshControl.endRefreshing()
			}
			dispatch($0, catch: { error in
				_ = app.reportError(error)
			}, or: {
				_ = x$($0)
			})
		}
	}
	
	func triggerRefresh() {
		refreshControl!.beginRefreshing()
		refresh()
	}
	
	func onceViewWillAppear() {
		try! fetchedResultsController.performFetch()
		triggerRefresh()
	}
	
	var scheduledForViewWillAppear = [() -> Void]()
	
	override func viewWillAppear(_ animated: Bool) {
		clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
		super.viewWillAppear(animated)
		
		scheduledForViewWillAppear.forEach { $0() }
		scheduledForViewWillAppear = []
	}

	// MARK: -
	
	@IBAction func newUser(_ sender: Any) {
		performSegue(withIdentifier: "newUser", sender: self)
	}

	// MARK: - Segues
	
	@IBAction func unwindForNewUser(_ segue: UIStoryboardSegue) {
		_ = x$(segue)
	}
	
	@IBAction func unwindForCancelNewUser(_ segue: UIStoryboardSegue) {
		_ = x$(segue)
	}

	@IBAction func unwindForUserDetail(_ segue: UIStoryboardSegue) {
		_ = x$(segue)
	}

	@IBAction func unwindForLogout(_ segue: UIStoryboardSegue) {
		_ = x$(segue)
	}
	
	// MARK: -

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		self.pushModel(to: segue)
		
		switch segue.identifier {
		case "showDetail"?:
		    if let indexPath = tableView.indexPath(for: sender as! UITableViewCell) {
				let user = fetchedResultsController.object(at: indexPath)
				push(user, to: segue.destination)
				(segue.destination as! UINavigationController).topViewController! … {
		        	$0.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
		        	$0.navigationItem.leftItemsSupplementBackButton = true
				}
		    }
		case "login"?:
			if let indexPath = tableView.indexPathForSelectedRow {
				let user = fetchedResultsController.object(at: indexPath)
				push(user, to: segue.destination)
			}
		default: ()
		}
	}

	// MARK: -

	func configureCell(_ cell: UITableViewCell, withUser user: User) {
		cell.textLabel!.text = user.displayName
	}
	
	func deleteUser(_ user: User) {
		cachedREST.delete(UserCachedRESTEntity.self, for: user) {
			dispatch($0, catch: {
				app.reportError($0)
			}, or: {
				_ = x$(user)
			})
		}
	}

	// MARK: -

	lazy var tableViewDataSource = FetchedResultsControllerTableViewDataSource(fetchedResultsController: fetchedResultsController, configureCell: configureCell, deleteObject: deleteUser)
	lazy var fetchedResultsControllerDelegate = TableViewFetchedResultsControllerDelegate(tableView: tableView, configureCell: configureCell)
	
	lazy var fetchedResultsController: NSFetchedResultsController<User> = {
		let fetchRequest: NSFetchRequest<User> = User.fetchRequest() … {
			$0.fetchBatchSize = 20
			$0.sortDescriptors = [
				NSSortDescriptor(key: #keyPath(User.lastName), ascending: true),
				NSSortDescriptor(key: #keyPath(User.firstName), ascending: true)
			]
			$0.predicate = NSPredicate(format: "(%K != TRUE) && (%K == TRUE)", #keyPath(User.deletedPendingSync), #keyPath(User.hasPrimitiveData))
		}
		
		return NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil) … {
			$0.delegate = fetchedResultsControllerDelegate
		}
	}()
}

