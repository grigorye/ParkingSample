//
//  DetailViewController.swift
//  ParkingSample
//
//  Created by Grigory Entin on 15.03.2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import UIKit

class UserDetailViewController : UserDetailBaseViewController {}

/// Common base for editing both new and existing user details.
class UserDetailBaseViewController : TableViewModelControllerBase, ModelController, CachedRESTConsumer, UserConsumer {

	var cachedREST: CachedREST!
	@objc dynamic var user: User!
	
	// MARK: -
	
	@IBOutlet var firstNameField: UITextField!
	@IBOutlet var lastNameField: UITextField!
	@IBOutlet var defaultVehicleLabel: UILabel!

	// MARK: -
	
	func modelDidChange() {
		guard let user = self.user else {
			return
		}
		firstNameField.text = user.firstName
		lastNameField.text = user.lastName
		defaultVehicleLabel.text = localizedDisplayName(for: user.defaultVehicle)
	}
	
	// MARK: -
	
	@objc static var keyPathsForValuesAffectingModel: Set<String> {
		return [
			#keyPath(user.defaultVehicle.title),
			#keyPath(user.firstName),
			#keyPath(user.lastName),
		]
	}
	
	// MARK: -

	func changeDefaultVehicle(to vehicle: Vehicle) throws {
		user.defaultVehicle = vehicle
		try cache.saveAsNecessary()
	}
	
	// MARK: -
	
    @IBAction func unwindForVehicleSelection(_ segue: UIStoryboardSegue) {
        _ = x$(segue)
    }
    
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		view.endEditing(true)
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		[firstNameField!, lastNameField].forEach {
			$0.delegate = self
		}
	}
	
	// MARK: -
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		pushModel(to: segue)
		if segue.identifier?.hasPrefix("unwind") ?? false {
			view.endEditing(true)
		}
		switch segue.identifier {
		case "selectVehicle"?:
			(segue.destination as! VehicleListViewController) … {
				$0.selectedVehicle = user.defaultVehicle
				$0.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
				$0.navigationItem.leftItemsSupplementBackButton = true
                let x = $0
				$0.selectionHandler = { [weak self, weak x] (vehicle) in
					do {
						try self!.changeDefaultVehicle(to: vehicle)
						x!.performSegue(withIdentifier: "unwindForVehicleSelection", sender: self)
					} catch {
						app.reportError(error)
					}
				}
			}
		default:
			()
		}
	}
	
	deinit {
		_ = x$(self)
	}
}

extension UserDetailBaseViewController : UITextFieldDelegate {
	
	func textFieldDidEndEditing(_ textField: UITextField) {
		let user = self.user!
		let updatedPrimtiveUser: PrimitiveUser = user.cachedPrimitiveObject()! … {
			$0.firstName = firstNameField.text
			$0.lastName = lastNameField.text
		}
		if x$(updatedPrimtiveUser != user.primitiveObject()) {
			cache.update(User.self, of: user, from: updatedPrimtiveUser) {
				dispatch($0, catch: { error in
					app.reportError(error)
				}, or: {
					_ = x$(user)
				})
			}
		}
	}
}
