//
//  TableViewModelControllerBase.swift
//  ParkingSample
//
//  Created by Grigory Entin on 28/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import UIKit

class TableViewModelControllerBase : UITableViewController {
	
	@objc private var model: AnyObject? {
		assert(false)
		return nil
	}
	
	private var modelObservation: NSKeyValueObservation!
	
	private func newModelObservation() -> NSKeyValueObservation {
		return observe(\.model, options: .initial) { [weak self] _, _ in
			self!.pushModelChange()
		}
	}
	
	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(x$(animated))
		modelObservation = nil
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(x$(animated))
		modelObservation = newModelObservation()
	}
}
