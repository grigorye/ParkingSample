//
//  UIViewController+ModelController.swift
//  ParkingSample
//
//  Created by Grigory Entin on 28/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import UIKit

extension UIViewController {
	
	func pushModelChange() {
		childConsumers.forEach { pushModel(to: $0) } // Quick hack/dependency on consumer model. Should replaced by injection of some kind.
		(self as! ModelController).modelDidChange()
	}
}
