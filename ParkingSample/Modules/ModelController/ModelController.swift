//
//  ModelController.swift
//  ParkingSample
//
//  Created by Grigory Entin on 28/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import UIKit

@objc protocol ModelController : NSObjectProtocol {
	
	func modelDidChange()
	@objc static var keyPathsForValuesAffectingModel: Set<String> { get }
}

extension ModelController {
	
	func pushModelChange() {
	}
}

extension ModelController where Self: Consumer /* */{
	
	func pushModelChange() {
	}
}
