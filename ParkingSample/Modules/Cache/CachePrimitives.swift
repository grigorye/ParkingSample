//
//  CachePrimitives.swift
//  ParkingSample
//
//  Created by Grigory Entin on 27/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import CoreData

extension Cache {
	
	func saveAsNecessary() throws {
		guard savesChanges else {
			return
		}
		try context.save()
	}

	func create<T>(_ entity: T.Type, from primitiveObject: T.PrimitiveRepresentation, completion: @escaping (() throws -> T) -> Void) where T: CachedObject & PrimitiveCodable {
		dispatch(completion) {
			let object = try T(context: context) … {
				try $0.cacheFrom(primitiveObject)
			}
			try saveAsNecessary()
			$0(object)
		}
	}
	
	func update<T>(_ entity: T.Type, of object: T, from primitiveObject: T.PrimitiveRepresentation, completion: @escaping (() throws -> Void) -> Void) where T: CachedObject & PrimitiveCodable {
		dispatch(completion) {
			try object.cacheFrom(primitiveObject)
			try saveAsNecessary()
			$0(())
		}
	}
	
	func delete<T>(_ object: T, completion: @escaping (() throws -> Void) -> Void) where T: CachedObject {
		dispatch(completion) {
			if nil == object.remoteObjectId {
				context.delete(object)
			} else {
				object.deletedPendingSync = true
			}
			try saveAsNecessary()
			$0(())
		}
	}
	
	func updateAll<T>(_ entity: T.Type, from remoteObjectsByObjectId: [RemoteObjectId : T.PrimitiveRepresentation], completion: @escaping (() throws -> Void) -> Void) where T: CachedObject & PrimitiveCodable {
		dispatch(completion) {
			assert(!savesChanges || !context.hasChanges)
			let remoteObjectIds = remoteObjectsByObjectId.map { Optional($0.key) }
			
			func fetchRequest() -> NSFetchRequest<T> { return NSFetchRequest(entityName: entity.entity().name!) }
			
			let deletedObjectsFetchRequest = fetchRequest() … {
				let remoteObjectIdKeyPath = #keyPath(CachedObject.remoteObjectId)
				$0.predicate = NSPredicate(
					format: "(%K != nil) && !(%K IN %@)", remoteObjectIdKeyPath, remoteObjectIdKeyPath, remoteObjectIds)
			}
			let deletedObjects = try context.fetch(x$(deletedObjectsFetchRequest))
			_ = x$(deletedObjects)
			
			deletedObjects.forEach {
				context.delete($0)
			}
			
			if x$(context.hasChanges) {
				try saveAsNecessary()
			}
			
			if _false {
				for (remoteObjectId, remoteObject) in remoteObjectsByObjectId {
					_ = try T(context: context) … {
						$0.remoteObjectId = remoteObjectId
						if $0.cachedPrimitiveObject() != remoteObject {
							try $0.cacheFrom(remoteObject)
						}
					}
				}
				
				try context.with(.mergeByPropertyObjectTrump) {
					try saveAsNecessary()
				}
			} else {
				let existingObjectsFetchRequest = fetchRequest() … {
					$0.predicate = NSPredicate(format: "%K != nil", #keyPath(CachedObject.remoteObjectId))
				}
				let existingObjects = try context.fetch(existingObjectsFetchRequest)
				
				try existingObjects.forEach {
					if let remoteObject = remoteObjectsByObjectId[$0.remoteObjectId!] {
						if $0.cachedPrimitiveObject() != remoteObject {
							try $0.cacheFrom(remoteObject)
							$0.remotelyChanged = true
						}
					}
				}
				
				let existingObjectIds = Set(existingObjects.map { $0.remoteObjectId! })
				let newObjectsByObjectId = remoteObjectsByObjectId.filter { (arg) in
					let (objectId, _) = arg
					return !existingObjectIds.contains(objectId)
				}
				for (objectId, newObject) in newObjectsByObjectId {
					_ = try T(context: context) … {
						$0.remoteObjectId = objectId
						try $0.cacheFrom(newObject)
						$0.remotelyChanged = true
					}
				}
				if x$(context.hasChanges) {
					try saveAsNecessary()
				}
			}
			$0(())
		}
	}
}
