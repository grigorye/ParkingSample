//
//  FlexibleCache.swift
//  ParkingSample
//
//  Created by Grigory Entin on 27/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import CoreData

struct FlexibleCache : Cache {
	let context: NSManagedObjectContext
}
