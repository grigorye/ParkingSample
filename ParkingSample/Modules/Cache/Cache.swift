//
//  Cache.swift
//  ParkingSample
//
//  Created by Grigory Entin on 20.03.2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import CoreData
import Foundation

protocol Cache {
	var context: NSManagedObjectContext { get }
	var savesChanges: Bool { get }
}

extension Cache {
	
	var savesChanges: Bool {
		return context.parent == nil
	}
}
