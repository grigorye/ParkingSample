//
//  CachedObjectOrRemoteObjectId.swift
//  ParkingSample
//
//  Created by Grigory Entin on 23/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import Foundation


enum CachedObjectOrRemoteObjectId<T: CachedObject> {
	
	case cachedObject(T)
	case remoteObjectId(RemoteObjectId)
	
	var remoteObjectId: RemoteObjectId? {
		switch self {
		case .remoteObjectId(let remoteObjectId):
			return remoteObjectId
		case .cachedObject(let cachedObject):
			return cachedObject.remoteObjectId
		}
	}
}

extension CachedObjectOrRemoteObjectId : Codable {
	
	init(from decoder: Decoder) throws {
		self = .remoteObjectId(try decoder.singleValueContainer().decode(RemoteObjectId.self))
	}
	
	func encode(to encoder: Encoder) throws {
		var container = encoder.singleValueContainer()
		switch self {
		case .remoteObjectId(let remoteObjectId):
			try container.encode(remoteObjectId)
		case .cachedObject(let cachedObject):
			try container.encode(cachedObject.remoteObjectId)
		}
	}
}

extension CachedObjectOrRemoteObjectId : Equatable {
	static func == (lhs: CachedObjectOrRemoteObjectId<T>, rhs: CachedObjectOrRemoteObjectId<T>) -> Bool {
		switch (lhs, rhs) {
		case (.cachedObject(let l), .cachedObject(let r)):
			return l.objectID == r.objectID
		default:
			return lhs.remoteObjectId == rhs.remoteObjectId
		}
	}
}
