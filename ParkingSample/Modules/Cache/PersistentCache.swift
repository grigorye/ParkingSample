//
//  PersistentCache.swift
//  ParkingSample
//
//  Created by Grigory Entin on 27/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import CoreData

struct PersistentCache : Cache {
	
	var context: NSManagedObjectContext {
		return container.viewContext
	}
	
	private static func newContainer(name: String) -> NSPersistentContainer {
		let container = NSPersistentContainer(name: name)
		container.loadPersistentStores(completionHandler: { (storeDescription, error) in
			container.viewContext.automaticallyMergesChangesFromParent = true
			
			if let error = error as NSError? {
				_ = x$(x$(error).userInfo)
				_ = x$(NSPersistentContainer.defaultDirectoryURL())
				fatalError()
			}
		})
		return container
	}
	
	private let container: NSPersistentContainer
	
	init(containerName: String) {
		container = type(of: self).newContainer(name: containerName)
	}
}
