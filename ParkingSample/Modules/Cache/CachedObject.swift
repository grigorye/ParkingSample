//
//  CachedObject.swift
//  ParkingSample
//
//  Created by Grigory Entin on 19.03.2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import CoreData.CoreDataErrors
import Foundation

extension CachedObject {
	
	public override func awakeFromInsert() {
		super.awakeFromInsert()
		createdAt = Date()
	}
	
	public override func validateForInsert() throws {
		guard !hasPrimitiveData else {
			try super.validateForInsert()
			return
		}
		if remoteObjectId == nil {
			let userInfo: [String : Any] = [
				NSValidationObjectErrorKey: self,
				NSValidationKeyErrorKey: #keyPath(remoteObjectId),
				NSLocalizedDescriptionKey: NSLocalizedString("Unsynced cached objects should have primitive data", comment: "")
			]
			throw CocoaError.error(.validationMissingMandatoryProperty, userInfo: userInfo)
		}
	}
	
	public override func willTurnIntoFault() {
		super.willTurnIntoFault()
		if _false {
			if let observationInfo = observationInfo {
				_ = x$(observationInfo)
			}
		}
	}
}
