//
//  PrimitiveCodable.swift
//  ParkingSample
//
//  Created by Grigory Entin on 27/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import Foundation

protocol PrimitiveCodable {
	
	associatedtype PrimitiveRepresentation: Equatable
	
	func setFrom(_ primitiveObject: PrimitiveRepresentation) throws
	func primitiveObject() -> PrimitiveRepresentation
}

extension PrimitiveCodable where Self : CachedObject {
	
	func cacheFrom(_ primitiveObject: PrimitiveRepresentation) throws {
		try setFrom(primitiveObject)
		hasPrimitiveData = true
	}
	
	func cachedPrimitiveObject() -> PrimitiveRepresentation? {
		guard hasPrimitiveData else {
			return nil
		}
		return primitiveObject()
	}
}
