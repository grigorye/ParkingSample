//
//  CachedObjectOrRemoteObjectIdPropeties.swift
//  ParkingSample
//
//  Created by Grigory Entin on 27/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import CoreData

extension PrimitiveCodable where Self : NSManagedObject {
	
	func unsavedValue<Value>(at keyPath: KeyPath<Self, Value>, forSettingFrom o: CachedObjectOrRemoteObjectId<Value>) throws -> Value {
		let oldValue = self[keyPath: keyPath]
		switch o {
		case .cachedObject(let user):
			return user
		case .remoteObjectId(let remoteObjectId):
			guard oldValue.remoteObjectId != remoteObjectId else {
				return oldValue
			}
			let fetchRequest: NSFetchRequest<Value> = NSFetchRequest(entityName: Value.entity().name!) … {
				$0.predicate = NSPredicate(format: "%K == %@", #keyPath(CachedObject.remoteObjectId), remoteObjectId)
			}
			let context = managedObjectContext!
			return try context.fetch(fetchRequest).first ?? Value(context: context) … {
				$0.remoteObjectId = remoteObjectId
			}
		}
	}
	
	func unsavedValue<Value>(at keyPath: KeyPath<Self, Value?>, forSettingFrom o: CachedObjectOrRemoteObjectId<Value>) throws -> Value {
		let oldValue = self[keyPath: keyPath]
		switch o {
		case .cachedObject(let user):
			return user
		case .remoteObjectId(let remoteObjectId):
			guard oldValue?.remoteObjectId != remoteObjectId else {
				return oldValue!
			}
			let fetchRequest: NSFetchRequest<Value> = NSFetchRequest(entityName: Value.entity().name!) … {
				$0.predicate = NSPredicate(format: "%K == %@", #keyPath(CachedObject.remoteObjectId), remoteObjectId)
			}
			let context = managedObjectContext!
			return try context.fetch(fetchRequest).first ?? Value(context: context) … {
				$0.remoteObjectId = remoteObjectId
			}
		}
	}
}
