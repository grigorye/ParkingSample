//
//  UIViewController+Consumer.swift
//  ParkingSample
//
//  Created by Grigory Entin on 28/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import UIKit.UIViewController

extension UIViewController : Consumer {
	var childConsumers: [Consumer] {
		return childViewControllers
	}
}
