//
//  DataPropagation.swift
//  ParkingSample
//
//  Created by Grigory Entin on 27/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

protocol Consumer : class {
	var childConsumers: [Consumer] { get }
}

extension Consumer {
	func forEachDescendantConsumer(andSelf includeSelf: Bool = false, _ block: (AnyObject) -> Void) {
		if includeSelf {
			block(self)
		}
		childConsumers.forEach {
			$0.forEachDescendantConsumer(andSelf: true, block)
		}
	}
}

private var modelPushers = [(Consumer, Consumer) -> Void]()

/// Injects pusher for the model.
func registerConsumerModelPusher(pusher: @escaping (Consumer, Consumer) -> Void) {
	modelPushers.append(pusher)
}

extension Consumer {
	
	func pushModel(to consumer: Consumer) {
		modelPushers.forEach {
			$0(self, consumer)
		}
	}
}
