//
//  Consumer+StoryboardSegues.swift
//  ParkingSample
//
//  Created by Grigory Entin on 28/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import UIKit

extension Consumer {
	
	func pushModel(to segue: UIStoryboardSegue) {
		let isUnwindSegue = !(segue.identifier?.hasPrefix("unwind") ?? false) // quck hack!
		guard isUnwindSegue else {
			_ = x$(segue.identifier)
			return
		}
		pushModel(to: segue.destination)
	}
}
