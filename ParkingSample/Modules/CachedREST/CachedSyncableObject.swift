//
//  CachedSyncableObject.swift
//  ParkingSample
//
//  Created by Grigory Entin on 27/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import CoreData.CoreDataErrors

extension CachedSyncableObject {
	
	@objc func isReadyToSync() -> Bool {
		guard hasPrimitiveData else {
			return false
		}
		guard nil == syncError else {
			return false
		}
		return true
	}
	
	@objc class func keysForSyncedRelationships() -> Set<String> {
		return []
	}
}

extension CachedSyncableObject {
	
	private func adjustForPendingSync() {
		guard hasPrimitiveData else {
			// Nothing can not be synced.
			return
		}
		guard !remotelyChanged else {
			// If it was import from remote, track it and ignore the pending sync (just once).
			pendingSync = false
			remotelyChanged = false
			return
		}
		// We rely on changing pendingSync just once (combined with remotelyChanged).
		guard !changedValues().keys.contains(#keyPath(pendingSync)) else {
			return
		}
		guard !pendingSync else {
			return
		}
		let changed: Bool = {
			let keysIgnoredForSync = CachedSyncableObject.entity().propertiesByName.keys
			let changedValueKeys = Set(changedValues().keys).filter { !keysIgnoredForSync.contains($0) }
			do {
				let keysForSyncedRelationships = type(of: self).keysForSyncedRelationships()
				guard nil == changedValueKeys.first(where: { keysForSyncedRelationships.contains($0) }) else {
					return true
				}
			}
			do {
				let keysForAttributes = entity.attributesByName.keys
				guard nil == changedValueKeys.first(where: { keysForAttributes.contains($0) }) else {
					return true
				}
			}
			return false
		}()
		guard changed else {
			return
		}
		pendingSync = true
	}
	
	public override func willSave() {
		super.willSave()
		adjustForPendingSync()
	}
	
	public override func validateForUpdate() throws {
		if isSyncing {
			let changedKeys = Set(changedValues().keys)
			if changedKeys != [#keyPath(isSyncing)] {
				let userInfo: [String : Any] = [
					NSValidationObjectErrorKey: self,
					NSValidationKeyErrorKey: #keyPath(isSyncing),
					"changedKeys": x$(changedKeys),
					NSLocalizedDescriptionKey: NSLocalizedString("Object should not be modified during sync", comment: "")
				]
				throw CocoaError.error(.validationMissingMandatoryProperty, userInfo: userInfo)
			}
		}
	}
}
