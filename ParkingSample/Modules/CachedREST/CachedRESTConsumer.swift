//
//  CachedRESTConsumer.swift
//  ParkingSample
//
//  Created by Grigory Entin on 27/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import CoreData
import Foundation

protocol CachedRESTConsumer : class {
	var cachedREST: CachedREST! { get set }
}

func push(_ cachedREST: CachedREST, to consumer: Consumer) {
	consumer.forEachDescendantConsumer(andSelf: true) {
		($0 as? CachedRESTConsumer)?.cachedREST = cachedREST
	}
}

extension CachedRESTConsumer {
	func pushCachedREST(to consumer: Consumer) {
		push(cachedREST, to: consumer)
	}
}

// MARK: - Conveniences

extension CachedRESTConsumer {
	
	var cache: Cache {
		return cachedREST.cache
	}
	
	var context: NSManagedObjectContext {
		return cachedREST.context
	}
}
