//
//  RESTCacheSynchronizer.swift
//  ParkingSample
//
//  Created by Grigory Entin on 26/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import CoreData
import Foundation

class RESTCacheSynchronizer : CachedRESTConsumer {
	
	var cachedREST: CachedREST!
	
	private var context: NSManagedObjectContext {
		return cache.context
	}

	var handlingContextDidSave = false
	func contextDidSave(_ notification: Notification) {
		guard !handlingContextDidSave else {
			return
		}
		handlingContextDidSave = true
		processPendingSync(onDidSave: notification)
		handlingContextDidSave = false
	}
	
	private var blocksForProgressPendingSyncOnDidSave = [(Notification) -> Void]()
	
	func register<T>(_ entity: T.Type) where T: CachedRESTEntity, T.CacheEntityT.PrimitiveRepresentation == T.RESTEntityT.PrimitiveRepresentation {
		blocksForProgressPendingSyncOnDidSave.append { [weak self] notification in
			self?.fetchAndSyncPending(entity) {
				dispatch($0, catch: { error in
					_ = x$(error)
				}, or: {
					_ = x$($0)
				})
			}
		}
	}
	
	func processPendingSync(onDidSave notification: Notification) {
		blocksForProgressPendingSyncOnDidSave.forEach {
			$0(notification)
		}
	}
	
	// MARK: -
	
	func fetchAndSyncPending<T>(_ entity: T.Type, completion: @escaping (() throws -> Void) -> Void) where T: CachedRESTEntity, T.CacheEntityT.PrimitiveRepresentation == T.RESTEntityT.PrimitiveRepresentation {
		dispatch(completion) {
			let entityFetchRequest: NSFetchRequest<T.CacheEntityT> = NSFetchRequest(entityName: T.CacheEntityT.entity().name!)
			let allObjects = try context.fetch(entityFetchRequest)
			_ = x$(allObjects.count)
			let fetchRequest: NSFetchRequest<T.CacheEntityT> = NSFetchRequest(entityName: T.CacheEntityT.entity().name!) … {
				$0.predicate = NSPredicate(format: "(%K == TRUE) && (%K != TRUE)", #keyPath(CachedSyncableObject.pendingSync), #keyPath(CachedSyncableObject.isSyncing))
			}
			let objectsPendingSync = try context.fetch(fetchRequest)
			for o in x$(objectsPendingSync) {
				guard !o.isSyncing else {
					continue
				}
				guard o.isReadyToSync() else {
					continue
				}
				if nil != o.remoteObjectId {
					cachedREST.scheduleUpdateRemote(entity, for: o) {
						dispatch($0, catch: { error in
							_ = x$(error)
						}, or: {
							_ = x$(o)
						})
					}
				} else {
					cachedREST.scheduleCreateRemote(entity, for: o) {
						dispatch($0, catch: { error in
							_ = x$(error)
						}, or: {
							_ = x$(o)
						})
					}
				}
			}
			$0(())
		}
	}
	
	// MARK: -
	
	private var saveMonitor: AnyObject!

	private var preparedForLaunch = false
	
	func prepareForLaunch() throws {
		let fetchRequest: NSFetchRequest<CachedSyncableObject> = CachedSyncableObject.fetchRequest() … {
			$0.predicate = NSPredicate(format: "%K == TRUE", #keyPath(CachedSyncableObject.isSyncing))
		}
		let orphanedSyncingObjects = try context.fetch(fetchRequest)
		x$(orphanedSyncingObjects).forEach {
			$0.isSyncing = false
		}
		try context.save()
		preparedForLaunch = true
	}
	
	func launch() {
		precondition(preparedForLaunch) // sanity
		precondition(nil == saveMonitor)
		saveMonitor = NotificationCenter.default.addObserver(forName: .NSManagedObjectContextDidSave, object: context, queue: nil) { [weak self] (notification) in
			self?.contextDidSave(notification)
		}
	}
	
	func stop() {
		precondition(nil != saveMonitor)
		saveMonitor = nil
	}
	
	init(cachedREST: CachedREST) {
		self.cachedREST = cachedREST
	}
}
