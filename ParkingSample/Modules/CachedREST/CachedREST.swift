//
//  CachedREST.swift
//  ParkingSample
//
//  Created by Grigory Entin on 20.03.2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import CoreData
import Foundation

struct CachedREST {
	let cache: Cache
	let connection: FirebaseCloudRESTConnection
	
	let baseURL: URL
	
	/// Fetch-only
	var context: NSManagedObjectContext {
		return cache.context
	}
	
	init(cache: Cache, baseURL: URL) {
		self.cache = cache
		self.baseURL = baseURL
		self.connection = FirebaseCloudRESTConnection(baseURL: baseURL)
	}
}

protocol CachedRESTEntity {
	associatedtype CacheEntityT: CachedSyncableObject & PrimitiveCodable
	associatedtype RESTEntityT: RESTEntity
}

extension CachedREST {
	
	func scheduleCreateRemote<T>(_ entity: T.Type, for cachedObject: T.CacheEntityT, completion: @escaping (() throws -> Void) -> Void) where T: CachedRESTEntity, T.CacheEntityT.PrimitiveRepresentation == T.RESTEntityT.PrimitiveRepresentation {
		precondition(cachedObject.remoteObjectId == nil)
		let cachedPrimitiveObject = cachedObject.cachedPrimitiveObject()!
		self.cache.willCreateRemote(cachedObject) {
			trap($0, completion) {
				self.connection.dispatchCommand(RESTCommand.Create<T.RESTEntityT>(primitiveObject: cachedPrimitiveObject)) {
					dispatch($0, catch: { error in
						cachedObject.managedObjectContext!.perform {
							self.cache.didFailToCreateRemote(cachedObject, error: error) {
								trap($0, completion, value: ())
							}
						}
					}, or: { remoteObjectId in
						cachedObject.managedObjectContext!.perform {
							self.cache.didCreateRemote(cachedObject, remoteObjectId: remoteObjectId) {
								trap($0, completion, value: ())
							}
						}
					})
				}
			}
		}
	}
	
	func scheduleUpdateRemote<T>(_ entity: T.Type, for cachedObject: T.CacheEntityT, completion: @escaping (() throws -> Void) -> Void) where T: CachedRESTEntity, T.CacheEntityT.PrimitiveRepresentation == T.RESTEntityT.PrimitiveRepresentation {
		precondition(cachedObject.remoteObjectId != nil)
		let cachedPrimitiveObject = cachedObject.cachedPrimitiveObject()!
		let remoteObjectId = cachedObject.remoteObjectId!
		self.cache.willUpdateRemote(cachedObject) {
			dispatch($0, completion) {
				self.connection.dispatchCommand(RESTCommand.Update<T.RESTEntityT>(primitiveObject: cachedPrimitiveObject, objectId: remoteObjectId)) {
					dispatch($0, catch: { error in
						cachedObject.managedObjectContext?.perform {
							self.cache.didFailToUpdateRemote(cachedObject, error: error) {
								trap($0, completion, value: ())
							}
						}
					}, or: {
						cachedObject.managedObjectContext?.perform {
							self.cache.didUpdateRemote(cachedObject, remoteObjectId: remoteObjectId) {
								trap($0, completion, value: ())
							}
						}
					})
				}
			}
		}

	}

	/// Creates (cached) object and schedules (potentially failing) synchronization with remote.
	func create<T>(_ entity: T.Type, from primitiveObject: T.CacheEntityT.PrimitiveRepresentation, completion: @escaping (() throws -> T.CacheEntityT) -> Void) where T: CachedRESTEntity, T.CacheEntityT.PrimitiveRepresentation == T.RESTEntityT.PrimitiveRepresentation {
		cache.create(T.CacheEntityT.self, from: primitiveObject) {
			dispatch($0, completion) { cachedObject in
				self.scheduleCreateRemote(entity, for: cachedObject) {
					trap($0, completion, value: cachedObject)
				}
			}
		}
	}
	
	/// Deletes (cached) object from cache and schedules (potentially failing) removal from the remote as necessary.
	func delete<T>(_ entity: T.Type, for cachedObject: T.CacheEntityT, completion: @escaping (() throws -> Void) -> Void) where T: CachedRESTEntity, T.CacheEntityT.PrimitiveRepresentation == T.RESTEntityT.PrimitiveRepresentation {
		cache.delete(cachedObject) {
			dispatch($0, completion) {
				guard cachedObject.deletedPendingSync else {
					completion({})
					return
				}
				self.cache.willDeleteRemote(cachedObject) {
					trap($0, completion) {
						self.connection.dispatchCommand(RESTCommand.Delete<UserRESTEntity>(objectId: cachedObject.remoteObjectId!)) {
							dispatch($0, catch: { error in
								cachedObject.managedObjectContext?.perform {
									self.cache.didFailToDeleteRemote(cachedObject, error: error) {
										trap($0, completion, value: ())
									}
								}
							}, or: {
								cachedObject.managedObjectContext?.perform {
									self.cache.didDeleteRemote(cachedObject) {
										trap($0, completion, value: ())
									}
								}
							})
						}
					}
				}
			}
		}
	}
	
	func refreshAll<T>(_ entity: T.Type, completion: @escaping (() throws -> Void) -> Void) where T: CachedRESTEntity, T.CacheEntityT.PrimitiveRepresentation == T.RESTEntityT.PrimitiveRepresentation {
		connection.dispatchCommand(RESTCommand.ReadAll<T.RESTEntityT>()) {
			dispatch($0, completion, follow: { objects in
				DispatchQueue.main.async {
					self.cache.updateAll(T.CacheEntityT.self, from: objects) {
						trap($0, completion, value: ())
					}
				}
			})
		}
	}
	
	/// Updates (cached) object and schedules (potentially failing) synchronization with remote.
	func update<T>(_ entity: T.Type, of cachedObject: T.CacheEntityT, from primitiveObject: T.RESTEntityT.PrimitiveRepresentation, completion: @escaping (() throws -> Void) -> Void) where T: CachedRESTEntity, T.CacheEntityT.PrimitiveRepresentation == T.RESTEntityT.PrimitiveRepresentation {
		cache.update(T.CacheEntityT.self, of: cachedObject, from: primitiveObject) {
			dispatch($0, completion) {
				self.scheduleUpdateRemote(entity, for: cachedObject) {
					trap($0, completion, value: ())
				}
			}
		}
	}
}
