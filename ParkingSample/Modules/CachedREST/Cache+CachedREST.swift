//
//  Cache+CachedREST.swift
//  ParkingSample
//
//  Created by Grigory Entin on 20.03.2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import CoreData
import Foundation

extension Cache {

	func willCreateRemote(_ object: CachedSyncableObject, completion: @escaping (() throws -> Void) -> Void) {
		dispatch(completion) {
			assert(!object.isSyncing)
			object.isSyncing = true
			_ = x$(object)
			try context.save()
			$0(())
		}
	}
	
	func didCreateRemote(_ object: CachedSyncableObject, remoteObjectId: String, completion: @escaping (() throws -> Void) -> Void) {
		dispatch(completion) {
			assert(object.isSyncing)
			assert(nil == object.remoteObjectId)
			object.isSyncing = false
			object.remoteObjectId = remoteObjectId
			object.remotelyChanged = true
			_ = x$(object)
			try context.save()
			$0(())
		}
	}
	
	func didFailToCreateRemote(_ object: CachedSyncableObject, error: Error, completion: @escaping (() throws -> Void) -> Void) {
		dispatch(completion) {
			assert(object.isSyncing)
			object.isSyncing = false
			object.syncError = error as NSError
			_ = x$(object)
			try context.save()
			$0(())
		}
	}
}

extension Cache {

	func willUpdateRemote(_ object: CachedSyncableObject, completion: @escaping (() throws -> Void) -> Void) {
		dispatch(completion) {
			assert(!object.isSyncing)
			assert(nil != object.remoteObjectId)
			object.isSyncing = true
			_ = x$(object)
			try context.save()
			$0(())
		}
	}
	
	func didUpdateRemote(_ object: CachedSyncableObject, remoteObjectId: String, completion: @escaping (() throws -> Void) -> Void) {
		dispatch(completion) {
			assert(object.isSyncing)
			assert(nil != object.remoteObjectId)
			object.isSyncing = false
			object.remoteObjectId = remoteObjectId
			object.remotelyChanged = true
			_ = x$(object)
			try context.save()
			$0(())
		}
	}
	
	func didFailToUpdateRemote(_ object: CachedSyncableObject, error: Error, completion: @escaping (() throws -> Void) -> Void) {
		dispatch(completion) {
			assert(object.isSyncing)
			object.isSyncing = false
			object.syncError = error as NSError
			_ = x$(object)
			try context.save()
			$0(())
		}
	}
}

extension Cache {

	func willDeleteRemote(_ object: CachedSyncableObject, completion: @escaping (() throws -> Void) -> Void) {
		dispatch(completion) {
			assert(!object.isSyncing)
			object.isSyncing = true
			_ = x$(object)
			try context.save()
			$0(())
		}
	}
	
	func didDeleteRemote(_ object: CachedSyncableObject, completion: @escaping (() throws -> Void) -> Void) {
		dispatch(completion) {
			_ = x$(object)
			assert(object.isSyncing)
			object.isSyncing = false
			context.delete(object)
			try context.save()
			$0(())
		}
	}
	
	func didFailToDeleteRemote(_ object: CachedSyncableObject, error: Error, completion: @escaping (() throws -> Void) -> Void) {
		dispatch(completion) {
			assert(object.isSyncing)
			object.isSyncing = false
			object.syncError = error as NSError
			_ = x$(object)
			try context.save()
			$0(())
		}
	}
}
