//
//  FirebaseCloudREST.swift
//  ParkingSample
//
//  Created by Grigory Entin on 15.03.2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import Foundation

/// Any parking specific command in context of Firebase Cloud REST.
protocol FirebaseCloudRESTCommand {

	associatedtype Result
	
	var requestPath: String { get }
	var httpMethod: String { get }
	var httpBody: Data? { get }
	var allHTTPHeaderFields: [String : String]? { get }

	// Extracts result from HTTP response and data
	func resultFromHTTPURLResponse(_ response: HTTPURLResponse, data: Data) throws -> Result

	typealias DispatchResult = () throws -> Result
}

extension FirebaseCloudRESTCommand {
	
	var httpMethod: String {
		return "GET"
	}
	
	var httpBody: Data? {
		return nil
	}
	
	var allHTTPHeaderFields: [String : String]? {
		return nil
	}
}

class FirebaseCloudRESTConnection {

	private let baseURL: URL
	
	init(baseURL: URL) {
		self.baseURL = baseURL
	}
	
	enum DispatchError : Error {
		case noData
		case badResponse(URLResponse?)
		case httpStatus(HTTPURLResponse, bodyOrData: BodyOrData)

		enum BodyOrData {
			case body(String)
			case data(Data)
		}
	}
	
	let session = URLSession(configuration: .default)

	/// Dispatches the given command, returning the command result in the completion.
	func dispatchCommand<BackendCommand>(_ command: BackendCommand, completionHandler: @escaping (BackendCommand.DispatchResult) -> ()) where BackendCommand: FirebaseCloudRESTCommand {
        let url = URL(string: command.requestPath, relativeTo: baseURL)!
		let request = URLRequest(url: x$(url)) … {
			$0.httpMethod = x$(command.httpMethod)
			$0.httpBody = command.httpBody
			$0.allHTTPHeaderFields = x$(command.allHTTPHeaderFields)
			$0.cachePolicy = .reloadIgnoringLocalAndRemoteCacheData
		}
		_ = x$(request.httpBody.flatMap { try? JSONSerialization.jsonObject(with: $0) })
		let dataTask = session.dataTask(with: request) { (data, response, error) in
			dispatch(completionHandler) {
				if let error = error {
					throw x$(error)
				}
				guard let data = data else {
					throw x$(DispatchError.noData)
				}
				guard let httpURLResponse = response as? HTTPURLResponse else {
					throw x$(DispatchError.badResponse(response))
				}
				guard httpURLResponse.statusCode == 200 else {
					let body = String(data: data, encoding: .utf8)
					throw x$(DispatchError.httpStatus(httpURLResponse, bodyOrData: body.flatMap { .body($0) } ?? .data(data)))
				}
				if let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) {
					_ = x$(json)
				} else {
					_ = x$(String(data: data, encoding: .utf8)!)
				}
				let result = try command.resultFromHTTPURLResponse(response as! HTTPURLResponse, data: data)
				$0(x$(result))
			}
		}
		_ = x$(dataTask.taskIdentifier)
		dataTask.resume()
	}
}
