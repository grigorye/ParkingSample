//
//  FirebaseCloudRESTCommands.swift
//  ParkingSample
//
//  Created by Grigory Entin on 17/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import Foundation

protocol RESTEntity {
	associatedtype PrimitiveRepresentation : Codable, Equatable

	static var resourceName: String { get }
}

typealias RemoteObjectId = String

enum RESTCommand {
	
	struct ReadAll<T> where T: RESTEntity {}
	
	struct Create<T> where T: RESTEntity {
		let primitiveObject: T.PrimitiveRepresentation
	}
	struct Read<T> where T: RESTEntity {
		let objectId: RemoteObjectId
	}
	
	struct Update<T> where T: RESTEntity {
		let primitiveObject: T.PrimitiveRepresentation
		let objectId: RemoteObjectId
	}
	
	struct Delete<T> where T: RESTEntity {
		let objectId: RemoteObjectId
	}
}

extension RESTCommand.Create : FirebaseCloudRESTCommand {
	typealias Result = RemoteObjectId
	
	var requestPath: String {
		return T.resourceName + ".json"
	}
	
	var httpMethod: String {
		return "POST"
	}
	
	var httpBody: Data? {
		return try! JSONEncoder().encode(primitiveObject)
	}
	
	func resultFromHTTPURLResponse(_ response: HTTPURLResponse, data: Data) throws -> RemoteObjectId {
		let json = try JSONDecoder().decode([String : String].self, from: data)
		return json["name"]!
	}
}

extension RESTCommand.Read : FirebaseCloudRESTCommand {
	typealias Result = T.PrimitiveRepresentation?
	
	var requestPath: String {
		return "\(T.resourceName)/\(objectId)/.json"
	}
	
	func resultFromHTTPURLResponse(_ response: HTTPURLResponse, data: Data) throws -> Result {
		guard data != nullFragmentJsonData else {
			return x$(nil)
		}
		
		let primitiveObject = try JSONDecoder().decode(T.PrimitiveRepresentation.self, from: data)
		return x$(primitiveObject)
	}
}

extension RESTCommand.Update : FirebaseCloudRESTCommand {
	typealias Result = Void
	
	var requestPath: String {
		return "\(T.resourceName)/\(objectId)/.json"
	}
	
	var httpMethod: String {
		return "PUT"
	}
	
	var httpBody: Data? {
		return try! JSONEncoder().encode(primitiveObject)
	}
	
	func resultFromHTTPURLResponse(_ response: HTTPURLResponse, data: Data) throws -> Result {
		return ()
	}
}

extension RESTCommand.Delete : FirebaseCloudRESTCommand {
	typealias Result = Void
	
	var requestPath: String {
		return "\(T.resourceName)/\(objectId)/.json"
	}
	
	var httpMethod: String {
		return "DELETE"
	}
	
	func resultFromHTTPURLResponse(_ response: HTTPURLResponse, data: Data) throws -> Result {
		assert(data == nullFragmentJsonData)
	}
}

extension RESTCommand.ReadAll : FirebaseCloudRESTCommand {
	typealias Result = [RemoteObjectId : T.PrimitiveRepresentation]
	
	var requestPath: String {
		return "\(T.resourceName).json"
	}
	
	var allHTTPHeaderFields: [String : String]? {
		return ["X-Firebase-ETag" : "true"]
	}
	
	func resultFromHTTPURLResponse(_ response: HTTPURLResponse, data: Data) throws -> Result {
		_ = x$(response.allHeaderFields["Etag"])
		
		guard data != nullFragmentJsonData else {
			return [:]
		}
		
		let primitiveObjects = try JSONDecoder().decode([RemoteObjectId : T.PrimitiveRepresentation].self, from: data)
		return primitiveObjects
	}
}
