//
//  CLLocationManagerExtensions.swift
//  ParkingSample
//
//  Created by Grigory Entin on 22/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import CoreLocation

public func string(from status: CLAuthorizationStatus) -> String {
	switch status {
	case .notDetermined:
		return ".notDetermined"
	case .restricted:
		return ".restricted"
	case .denied:
		return ".denied"
	case .authorizedAlways:
		return ".authorizedAlways"
	case .authorizedWhenInUse:
		return ".authorizedWhenIdle"
	}
}
