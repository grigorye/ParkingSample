//
//  Tracing.swift
//  ParkingSample
//
//  Created by Grigory Entin on 24/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import Foundation

private func xx$<T>(_ v: T) -> String {
	var s = ""
	debugPrint(v, to: &s)
	return s
}

private func printDumped(_ type: String, _ s: String, function: String, file: String, line: Int) {
	print("\(URL(fileURLWithPath: file).lastPathComponent):\(line)|\(function):\n\(s)")
}

func x$<T>(_ v: T?, function: String = #function, file: String = #file, line: Int = #line) -> T? {
	printDumped("\(type(of: v))", xx$(v), function: function, file: file, line: line)
	return v
}

func x$<T>(_ v: T, function: String = #function, file: String = #file, line: Int = #line) -> T {
	printDumped("\(type(of: v))", xx$(v), function: function, file: file, line: line)
	return v
}

// MARK: -

func nnilx$<T>(_ v: T?, function: String = #function, file: String = #file, line: Int = #line) -> T? {
	guard v != nil else {
		return nil
	}
	return x$(v, function: function, file: file, line: line)
}
