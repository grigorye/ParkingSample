//
//  NSFetchedResultsControllerExtensions.swift
//  ParkingSample
//
//  Created by Grigory Entin on 21/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import CoreData

public func string(from type: NSFetchedResultsChangeType) -> String {
	switch type {
	case .insert:
		return ".insert"
	case .delete:
		return ".delete"
	case .update:
		return ".update"
	case .move:
		return ".move"
	}
}
