//
//  MapViewFetchedResultsControllerDelegate.swift
//  ParkingSample
//
//  Created by Grigory Entin on 22/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import MapKit
import CoreData

/// Makes given fetched results displayed on a map.
class MapViewFetchedResultsControllerDelegate : NSObject, NSFetchedResultsControllerDelegate {
	
	var mapView: MKMapView
	
	init(for mapView: MKMapView) {
		self.mapView = mapView
	}
	
	func controllerDidFetchObjects(_ controller: NSFetchedResultsController<ParkingZone>) {
		assert(0 == mapView.annotations.count)
		mapView.addAnnotations(controller.fetchedObjects!)
	}
	
	func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
		let controller = controller as! NSFetchedResultsController<ParkingZone>
		_ = x$(string(from: type))
		switch type {
		case .delete:
			mapView.removeAnnotation(controller.object(at: indexPath!))
		case .insert:
			mapView.addAnnotation(controller.object(at: newIndexPath!))
		default:
			()
		}
	}
}
