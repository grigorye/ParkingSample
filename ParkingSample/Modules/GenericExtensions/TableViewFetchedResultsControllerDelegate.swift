//
//  TableViewFetchedResultsControllerDelegate.swift
//  ParkingSample
//
//  Created by Grigory Entin on 23/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import UIKit
import CoreData

class TableViewFetchedResultsControllerDelegate<T> : NSObject, NSFetchedResultsControllerDelegate where T: Any {
	
	let tableView: UITableView
	let configureCell: (UITableViewCell, T) -> Void
	
	init(tableView: UITableView, configureCell: @escaping (UITableViewCell, T) -> Void) {
		self.tableView = tableView
		self.configureCell = configureCell
	}
	
	// MARK: -
	
	func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
		tableView.beginUpdates()
	}
	
	func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
		switch type {
		case .insert:
			tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
		case .delete:
			tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
		default:
			return
		}
	}
	
	func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
		_ = (x$(string(from: type)), x$(indexPath), x$(newIndexPath))
		switch type {
		case .insert:
			tableView.insertRows(at: [newIndexPath!], with: .fade)
		case .delete:
			tableView.deleteRows(at: [indexPath!], with: .fade)
		case .move:
			if let cell = tableView.cellForRow(at: indexPath!) {
				configureCell(cell, anObject as! T)
			}
			tableView.moveRow(at: indexPath!, to: newIndexPath!)
		case .update:
			if let cell = tableView.cellForRow(at: indexPath!) {
				configureCell(x$(cell), anObject as! T)
			}
		}
	}
	
	func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
		tableView.endUpdates()
	}
}
