//
//  NSManagedObjectContextExtensions.swift
//  ParkingSample
//
//  Created by Grigory Entin on 20/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import CoreData

extension NSManagedObjectContext {
	func with(_ mergePolicy: NSMergePolicy, _ block: () throws -> Void) rethrows {
		let savedMergePolicy = self.mergePolicy
		self.mergePolicy = mergePolicy
		defer { self.mergePolicy = savedMergePolicy }
		try block()
	}
}
