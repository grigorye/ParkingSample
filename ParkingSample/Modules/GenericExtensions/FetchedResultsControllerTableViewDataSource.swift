//
//  FetchedResultsControllerTableViewDataSource.swift
//  ParkingSample
//
//  Created by Grigory Entin on 23/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import UIKit
import CoreData
import Foundation

class FetchedResultsControllerTableViewDataSource<T: NSFetchRequestResult> : NSObject, UITableViewDataSource {
	
	let fetchedResultsController: NSFetchedResultsController<T>
	let configureCell: (UITableViewCell, T) -> Void
	let deleteObject: (T) -> Void
	
	init(fetchedResultsController: NSFetchedResultsController<T>, configureCell: @escaping (UITableViewCell, T) -> Void, deleteObject: @escaping (T) -> Void) {
		self.fetchedResultsController = fetchedResultsController
		self.configureCell = configureCell
		self.deleteObject = deleteObject
	}
	
	// MARK: -
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return fetchedResultsController.sections?.count ?? 0
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		let sectionInfo = fetchedResultsController.sections![section]
		return sectionInfo.numberOfObjects
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
		let object = fetchedResultsController.object(at: indexPath)
		configureCell(cell, object)
		return cell
	}
	
	func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
		return true
	}
	
	func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
		if editingStyle == .delete {
			let object = fetchedResultsController.object(at: indexPath)
			deleteObject(object)
		}
	}
}
