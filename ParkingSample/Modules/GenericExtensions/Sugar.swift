//
//  Sugar.swift
//  ParkingSample
//
//  Created by Grigory Entin on 16.03.18.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import Foundation

infix operator …: MultiplicationPrecedence

@discardableResult
func …<T>(_ value: T, apply: (inout T) throws -> ()) rethrows -> T {
	var modifiedValue = value
	try apply(&modifiedValue)
	return modifiedValue
}

let nullFragmentJsonData = "null".data(using: .utf8)

var _false = false
