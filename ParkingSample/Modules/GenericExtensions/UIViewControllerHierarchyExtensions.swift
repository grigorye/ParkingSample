//
//  UIViewControllerHierarchyExtensions.swift
//  ParkingSample
//
//  Created by Grigory Entin on 27/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import UIKit.UIViewController

extension UIViewController {
	
	func descendant<T: UIViewController>() -> T! {
		if let matchingSelf = x$(self) as? T {
			return matchingSelf
		}
		for child in childViewControllers {
			if let matchingChildDescendant = child.descendant() as T? {
				return matchingChildDescendant
			}
		}
		return nil
	}
}
