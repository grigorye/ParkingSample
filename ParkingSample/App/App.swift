//
//  App.swift
//  ParkingSample
//
//  Created by Grigory Entin on 23/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import Foundation.NSURL

struct App {}

extension App {

	func reportError(_ error: Error) {
		_ = x$(error)
	}
}

var app = App()

private let cache: Cache = PersistentCache(containerName: "ParkingSample")
private let cachedREST = CachedREST(cache: cache, baseURL: URL(string: "https://testapi-11cc3-657ec.firebaseio.com")!)

let defaultCachedREST = cachedREST
