//
//  UserAppEntityExtensions.swift
//  ParkingSample
//
//  Created by Grigory Entin on 24/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

extension User {
	
	func vehicleWithId(_ vehicleId: RemoteObjectId) -> Vehicle? {
		return (vehicles as! Set<Vehicle>).first { $0.remoteObjectId == vehicleId }
	}
}
