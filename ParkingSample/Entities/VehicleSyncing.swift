//
//  VehicleSyncing.swift
//  ParkingSample
//
//  Created by Grigory Entin on 26/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

extension Vehicle {
	
	override func isReadyToSync() -> Bool {
		guard super.isReadyToSync() else {
			return false
		}
		return user?.remoteObjectId != nil
	}
	
	override class func keysForSyncedRelationships() -> Set<String> {
		return [#keyPath(defaultUser), #keyPath(user)]
	}
}
