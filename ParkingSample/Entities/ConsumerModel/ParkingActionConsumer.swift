//
//  ParkingActionConsumer.swift
//  ParkingSample
//
//  Created by Grigory Entin on 27/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

protocol ParkingActionConsumer : Consumer {
	var parkingAction: ParkingAction! { get set }
}

extension ParkingActionConsumer {
	func pushParkingAction(to consumer: Consumer) {
		push(parkingAction, to: consumer)
	}
}

func push(_ parkingAction: ParkingAction!, to consumer: Consumer) {
	consumer.forEachDescendantConsumer(andSelf: true) {
		($0 as? ParkingActionConsumer)?.parkingAction = parkingAction
	}
}
