//
//  UserConsumer.swift
//  ParkingSample
//
//  Created by Grigory Entin on 27/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

protocol UserConsumer : class {
	var user: User! { get set }
}

extension UserConsumer {
	func pushUser(to consumer: Consumer) {
		push(user, to: consumer)
	}
}

func push(_ user: User, to consumer: Consumer) {
	consumer.forEachDescendantConsumer(andSelf: true) {
		($0 as? UserConsumer)?.user = user
	}
}
