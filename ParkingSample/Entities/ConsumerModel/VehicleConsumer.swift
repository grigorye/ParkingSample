//
//  VehicleConsumer.swift
//  ParkingSample
//
//  Created by Grigory Entin on 27/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

protocol VehicleConsumer : Consumer {
	var vehicle: Vehicle! { get set }
}

extension VehicleConsumer {
	func pushVehicle(to consumer: Consumer) {
		push(vehicle, to: consumer)
	}
}

func push(_ vehicle: Vehicle!, to consumer: Consumer) {
	consumer.forEachDescendantConsumer(andSelf: true) {
		($0 as? VehicleConsumer)?.vehicle = vehicle
	}
}
