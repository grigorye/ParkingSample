//
//  ParkingZoneConsumer.swift
//  ParkingSample
//
//  Created by Grigory Entin on 27/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

protocol ParkingZoneConsumer : Consumer {
	var parkingZone: ParkingZone! { get set }
}

extension ParkingZoneConsumer {
	func pushParkingZone(to consumer: Consumer) {
		push(parkingZone, to: consumer)
	}
}

func push(_ parkingZone: ParkingZone!, to consumer: Consumer) {
	consumer.forEachDescendantConsumer(andSelf: true) {
		nnilx$(($0 as? ParkingZoneConsumer))?.parkingZone = parkingZone
	}
}
