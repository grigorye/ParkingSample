//
//  Entities+PrimitiveCodable.swift
//  ParkingSample
//
//  Created by Grigory Entin on 21/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

extension User : PrimitiveCodable {
	
	typealias PrimitiveRepresentation = PrimitiveUser
	
	func setFrom(_ p: PrimitiveRepresentation) throws {
		if userName != p.userName {
			userName = p.userName
		}
		if firstName != p.firstName {
			firstName = p.firstName
		}
		if lastName != p.lastName {
			lastName = p.lastName
		}
	}
	
	func primitiveObject() -> PrimitiveRepresentation {
		return PrimitiveRepresentation(userName: userName, firstName: firstName, lastName: lastName)
	}
}

extension ParkingZone : PrimitiveCodable {
	
	typealias PrimitiveRepresentation = PrimitiveParkingZone
	
	func setFrom(_ p: PrimitiveRepresentation) {
		lat = p.lat; lon = p.lon; tariff = p.tariff; address = p.address
	}
	
	func primitiveObject() -> PrimitiveRepresentation {
		return PrimitiveRepresentation(lat: lat, lon: lon, tariff: tariff, address: address)
	}
}

extension ParkingAction : PrimitiveCodable {
	
	typealias PrimitiveRepresentation = PrimitiveParkingAction
	
	func setFrom(_ p: PrimitiveRepresentation) throws {
		vehicleId = p.vehicleId; zoneId = p.zoneId; startDate = p.startDate; stopDate = p.stopDate
		let newUser = try unsavedValue(at: \.user, forSettingFrom: p.userIdOrUser)
		if newUser != user {
			user = x$(newUser)
		}
	}
	
	func primitiveObject() -> PrimitiveRepresentation {
		return PrimitiveRepresentation(userIdOrUser: .cachedObject(user!), vehicleId: vehicleId!, zoneId: zoneId!, startDate: startDate!, stopDate: stopDate)
	}
}

extension Vehicle : PrimitiveCodable {
	
	typealias PrimitiveRepresentation = PrimitiveVehicle
	
	func setFrom(_ p: PrimitiveRepresentation) throws {
		vrn = p.vrn; title = p.title
		let newUser = try unsavedValue(at: \.user, forSettingFrom: p.userIdOrUser)
		if newUser != user {
			user = x$(newUser)
		}
		defaultUser = (p.isDefault ?? false) ? newUser : nil
	}
	
	func primitiveObject() -> PrimitiveRepresentation {
		return PrimitiveRepresentation(userIdOrUser: .cachedObject(user!), vrn: vrn, title: title, isDefault: (defaultUser != nil) ? true : nil)
	}
}
