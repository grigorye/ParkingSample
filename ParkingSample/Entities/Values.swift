//
//  Values.swift
//  ParkingSample
//
//  Created by Grigory Entin on 20.03.2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

import Foundation.NSDate

struct PrimitiveUser : Codable, Equatable {
	var userName: String?
	var firstName: String?
	var lastName: String?
}

struct PrimitiveParkingZone : Codable, Equatable {
	var lat: Float
	var lon: Float
	var tariff: String?
	var address: String?
}

struct PrimitiveVehicle : Codable, Equatable {
	
	private enum CodingKeys : String, CodingKey {
		case userIdOrUser = "userId", vrn, title, isDefault
	}
	
	var userIdOrUser: CachedObjectOrRemoteObjectId<User>
	var vrn: String?
	var title: String?
	var isDefault: Bool?
}

struct PrimitiveParkingAction : Codable, Equatable {
	
	private enum CodingKeys : String, CodingKey {
		case userIdOrUser = "userId", vehicleId, zoneId, startDate, stopDate
	}

	var userIdOrUser: CachedObjectOrRemoteObjectId<User>
	var vehicleId: RemoteObjectId
	var zoneId: RemoteObjectId
	var startDate: Date
	var stopDate: Date?
}
