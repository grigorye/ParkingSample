//
//  RESTEntities.swift
//  ParkingSample
//
//  Created by Grigory Entin on 21/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

struct ParkingZoneRESTEntity : RESTEntity {
	typealias PrimitiveRepresentation = PrimitiveParkingZone
	static let resourceName = "parkingZones"
}

struct UserRESTEntity : RESTEntity {
	typealias PrimitiveRepresentation = PrimitiveUser
	static let resourceName = "users"
}

struct VehicleRESTEntity : RESTEntity {
	typealias PrimitiveRepresentation = PrimitiveVehicle
	static let resourceName = "vehicles"
}

struct ParkingActionRESTEntity : RESTEntity {
	typealias PrimitiveRepresentation = PrimitiveParkingAction
	static let resourceName = "parkingActions"
}
