//
//  AppObjectEntities.swift
//  ParkingSample
//
//  Created by Grigory Entin on 21/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

struct UserCachedRESTEntity : CachedRESTEntity {
	typealias RESTEntityT = UserRESTEntity
	typealias CacheEntityT = User
}

struct ParkingZoneCachedRESTEntity : CachedRESTEntity {
	typealias RESTEntityT = ParkingZoneRESTEntity
	typealias CacheEntityT = ParkingZone
}

struct ParkingActionCachedRESTEntity : CachedRESTEntity {
	typealias RESTEntityT = ParkingActionRESTEntity
	typealias CacheEntityT = ParkingAction
}

struct VehicleCachedRESTEntity : CachedRESTEntity {
	typealias RESTEntityT = VehicleRESTEntity
	typealias CacheEntityT = Vehicle
}
