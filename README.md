[![](https://gitlab.com/grigorye/ParkingSample/badges/master/pipeline.svg)](https://gitlab.com/grigorye/ParkingSample/commits/master)

# ParkingSample

An attempt to build a sample REST client with nothing but standard frameworks.

Requires at least Xcode 9.3/Swift 4.1. Doesn't build with Xcode 9.2, sorry.
Not tested on anything below iOS 11.2.6.

## Modules

There're a few modules that deal with some generic aspect of the implementation:

* *Backend*: REST client for Firebase (JSON).
* *Cache*: REST cache implementation based on Core Data, with support for relatinoships not yet retrieved from the backend.
* *CachedREST*: A combination of Cache and Backend that is tailored for *automatic* synchronization. You save data in cache, it gets pushed to the backend automatically (and vice versa).
* *ModelController*: Manageable (data) model for view controllers.
* *ConsumerModel*: Data flow model, streamlined for passing data between view controllers.

## Design Decisions

### Overall Structure

Everything is controlled by view controllers, no other "active" components exist (except for the cached REST synchronizer and etc.) that manipulates the data.

### Routing

Routing is mostly defined by the storyboard. While initially it might sound "bad", I believe for this particular case, storyboard is enough and provides quite flexible manageable solution, given that the support from the code side is kept to the minimum.

### View Controller Data Flow

Dependency injection is there for view controllers. There're no shared/global components available at any level. The mechanizm for injection relies on the assumption that the data flows only down and in just two ways:

1. To children controllers (as defined by the model)
2. To presented controllers (at the time of the presentation).

To support such an injection/flow, there's notion of Consumer. The propagation of the data is done automatically, when possbile, through the chain of children controllers (triggered by KVO on the model) and presented controllers (on `prepare(for:segue)`).

Hence if a child/presented controller is a consumer of the same data type as the parent/presenting controller, the data will be automatically propagated to the child/presented controller, as necessary. When parent/presenting controller is not consumer of the same data type as the child/presented controller, it provides the data explicitly at the time of presentation or e.g. as part of data model tracking.

### Persistent/User Level Data

There's nothing there except for Core Data entities. E.g. default vehicle is made part of the REST (User) object that is represented by (User) Core Data entity.

### REST Caching/Synchronization

It was kind of natural to develop a generic solution for this.

## Current State

1. REST synchronization is not complete, e.g. deletion is still manually triggered, there're some bugs (at least it looks like that) related to dealing with the data that is currently being synced and etc. It should be further investigated/there's quite big room for that.
2. Overall, generic APIs are not that optimal, might be improved.
3. Unit tests are not there. Sorry. The only thing that has the tests is REST/Firebase communication module.
4. It should work offline. However, the sync is not triggered e.g. on restored Internet connection, but just on a data change.
5. Default vehicle is not tracked if the vehicle has to be retrived from the backend (currently it's done only when you navigate to a vehicle list). A simple solution would be retrieving the list of default vehicles at the same time the list of users is retrived, like in `/vehicles.json?orderBy="isDefault"&equalTo=1`.
6. There're quite a few unfinished things here and there.
