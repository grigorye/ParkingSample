//
//  XCTestCase+FirebaseCloudREST.swift
//  ParkingSampleTests
//
//  Created by Grigory Entin on 21/03/2018.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

@testable import ParkingSample
import XCTest

extension XCTestCase {
	
	func testCRUD<T>(for RESTEntity: T.Type, sample: T.PrimitiveRepresentation, modifiedSample: T.PrimitiveRepresentation, baseURL: URL) where T: RESTEntity {
		let connection = FirebaseCloudRESTConnection(baseURL: baseURL)
		
		// READ-ALL-INITIAL
		let initialObjects: [RemoteObjectId : T.PrimitiveRepresentation]
		do {
			var dispatchedObjectsRead: [RemoteObjectId : T.PrimitiveRepresentation]?
			let dispatched = expectation(description: "Read Initial All \(T.self)")
			connection.dispatchCommand(RESTCommand.ReadAll<T>()) {
				defer { dispatched.fulfill() }
				dispatch($0, catch: { XCTFail("\($0)") }) {
					dispatchedObjectsRead = $0
				}
			}
			waitForExpectations(timeout: 10)
			guard let objects = dispatchedObjectsRead else { return }
			initialObjects = objects
		}
		
		// CREATE
		let objectId: RemoteObjectId
		do {
			var dispatchedCreatedObjectId: RemoteObjectId?
			let dispatched = expectation(description: "Created New \(T.self)")
			connection.dispatchCommand(RESTCommand.Create<T>(primitiveObject: sample)) {
				defer { dispatched.fulfill() }
				dispatch($0, catch: { XCTFail("\($0)") }) {
					dispatchedCreatedObjectId = $0
				}
			}
			waitForExpectations(timeout: 10)
			guard let createdObjectId = dispatchedCreatedObjectId else { return }
			objectId = createdObjectId
		}
		
		// READ-CREATED
		do {
			var dispatchedObjectRead: T.PrimitiveRepresentation?
			let dispatched = expectation(description: "Read Created \(T.self)")
			connection.dispatchCommand(RESTCommand.Read<T>(objectId: objectId)) {
				defer { dispatched.fulfill() }
				dispatch($0, catch: { XCTFail("\($0)") }) {
					dispatchedObjectRead = $0
				}
			}
			waitForExpectations(timeout: 10)
			guard let objectRead = dispatchedObjectRead else { return }
			XCTAssertEqual(sample, objectRead)
		}
		
		// READ-ALL-AFTER-CREATE
		do {
			var dispatchedAllObjectsRead: [RemoteObjectId : T.PrimitiveRepresentation]?
			let dispatched = expectation(description: "Read All \(T.self) After Creation")
			connection.dispatchCommand(RESTCommand.ReadAll<T>()) {
				defer { dispatched.fulfill() }
				dispatch($0, catch: { XCTFail("\($0)") }) {
					dispatchedAllObjectsRead = $0
				}
			}
			waitForExpectations(timeout: 10)
			guard let objectsWithCreated = dispatchedAllObjectsRead else { return }
			XCTAssertEqual(objectsWithCreated.count, initialObjects.count + 1)
		}
		
		// UPDATE
		do {
			let dispatched = expectation(description: "Updated User")
			connection.dispatchCommand(RESTCommand.Update<T>(primitiveObject: modifiedSample, objectId: objectId)) {
				defer { dispatched.fulfill() }
				dispatch($0, catch: { XCTFail("\($0)") })
			}
			waitForExpectations(timeout: 10)
		}
		
		// READ-UPDATED
		do {
			var dispatchedObjectRead: T.PrimitiveRepresentation?
			let dispatched = expectation(description: "Read Updated \(T.self)")
			connection.dispatchCommand(RESTCommand.Read<T>(objectId: objectId)) {
				defer { dispatched.fulfill() }
				dispatch($0, catch: { XCTFail("\($0)") }) {
					dispatchedObjectRead = $0
				}
			}
			waitForExpectations(timeout: 10)
			guard let updatedObject = dispatchedObjectRead else { return }
			XCTAssertEqual(modifiedSample, updatedObject)
		}
		
		// DELETE
		do {
			let dispatched = expectation(description: "Deleted \(T.self)")
			connection.dispatchCommand(RESTCommand.Delete<T>(objectId: objectId)) {
				defer { dispatched.fulfill() }
				dispatch($0, catch: { XCTFail("\($0)") })
			}
			waitForExpectations(timeout: 10)
		}
		
		// READ-DELETED
		do {
			var dispatchedObjectRead: T.PrimitiveRepresentation?
			let dispatched = expectation(description: "Read Deleted \(T.self)")
			connection.dispatchCommand(RESTCommand.Read<T>(objectId: objectId)) {
				defer { dispatched.fulfill() }
				dispatch($0, catch: { XCTFail("\($0)") }) {
					dispatchedObjectRead = $0
				}
			}
			waitForExpectations(timeout: 10)
			let deletedObject = dispatchedObjectRead
			XCTAssertNil(deletedObject)
		}
		
		// READ-ALL-AFTER-CREATE
		do {
			var dispatchedAllObjectsRead: [RemoteObjectId : T.PrimitiveRepresentation]?
			let dispatched = expectation(description: "Read All \(T.self) After Deletion")
			connection.dispatchCommand(RESTCommand.ReadAll<T>()) {
				defer { dispatched.fulfill() }
				dispatch($0, catch: { XCTFail("\($0)") })
			}
			waitForExpectations(timeout: 10)
			guard let objectsWithoutDeleted = dispatchedAllObjectsRead else { return }
			XCTAssertEqual(objectsWithoutDeleted.count, initialObjects.count)
		}
	}
}
