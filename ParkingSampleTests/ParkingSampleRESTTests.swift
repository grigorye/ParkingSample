//
//  ParkingSampleRESTTests.swift
//  ParkingSample
//
//  Created by Grigory Entin on 16.03.18.
//  Copyright © 2018 Grigory Entin. All rights reserved.
//

@testable import ParkingSample
import XCTest

class ParkingSampleRESTTests: XCTestCase {
	
	let baseURL = URL(string: "https://testapi-11cc3-657ec.firebaseio.com")!
	
	func testUserCRUD() {
		let user = PrimitiveUser() … {
			$0.firstName = "John"
			$0.lastName = nil
			$0.userName = "johndoe"
		}
		
		let modifiedUser = user … {
			$0.lastName = "Doe"
			$0.userName = nil
		}
		
		testCRUD(for: UserRESTEntity.self, sample: user, modifiedSample: modifiedUser, baseURL: baseURL)
	}
	
	func testParkingZoneCRUD() {
		let zone = PrimitiveParkingZone(
			lat: 52.3268841,
			lon: 4.9499596,
			tariff: "4 eur/hr",
			address: "Amsterdam-Zuidoost, Develstein 100"
		)
		
		let modifiedZone = zone … {
			$0.tariff = "5 eur/hr"
		}
		
		testCRUD(for: ParkingZoneRESTEntity.self, sample: zone, modifiedSample: modifiedZone, baseURL: baseURL)
	}
	
	func testVehicleCRUD() {
		let vehicle = PrimitiveVehicle(
			userIdOrUser: .remoteObjectId("-L83mYlTlD4x2ExY20b6"),
			vrn: "XXX-XXX",
			title: "My Car",
			isDefault: nil
		)
		
		let modifiedVehicle = vehicle … {
			$0.title = "Her car"
		}
		
		testCRUD(for: VehicleRESTEntity.self, sample: vehicle, modifiedSample: modifiedVehicle, baseURL: baseURL)
	}
	
	func testParkingActionCRUD() {
		let parkingAction = PrimitiveParkingAction(
			userIdOrUser: .remoteObjectId("-L83mYlTlD4x2ExY20b6"),
			vehicleId: "-L88BDNdaXrijgRQFcNJ",
			zoneId: "-L85A8B64it5T4YoFNg4",
			startDate: Date(),
			stopDate: nil
		)
		
		let modifiedParkingAction = parkingAction … {
			$0.stopDate = Date()
		}
		
		testCRUD(for: ParkingActionRESTEntity.self, sample: parkingAction, modifiedSample: modifiedParkingAction, baseURL: baseURL)
	}
}
